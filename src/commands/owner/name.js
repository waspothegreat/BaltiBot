exports.run = (client, msg) => {
    const name = msg.join(' ');
    client.user.setUsername(name)
        .then(() => {
            msg.replyOk(msg.channel, 'Username changed successfully!');
        })
        .catch(e => {
            msg.replyErr(msg.channel, `Oh no, something went wrong!\n\n${e}`);
        });
};

exports.help = { name: 'setname', usage: '{prefix}setname <name>', desc: 'Set the bots name.', aliases: [], USER_PERMS: [], BOT_PERMS: [], public: false, premium: false };