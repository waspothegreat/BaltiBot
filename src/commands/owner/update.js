const exec = require('@freebroccolo/shelljs-promises').exec;

exports.run = async (client, msg) => {
    let m = await msg.channel.send(`Running \`git pull\`...`);
    await exec('git pull');
    await m.edit(`Running \`${process.env.UPDATE_COMMAND}\`...`);
    await exec(process.env.UPDATE_COMMAND);
    await m.edit('Restarting...');
    await client.destroy();
    process.exit(2); //eslint-disable-line
};

exports.help = { name: 'update', usage: '{prefix}update', desc: 'Update the bot.', aliases: [], USER_PERMS: [], BOT_PERMS: [], public: false, premium: false };