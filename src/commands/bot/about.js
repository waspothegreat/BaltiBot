const { RichEmbed } = require('discord.js');

exports.run = async (client, msg) => { // jshint ignore:line
    let multipleOwners = client.owners.length > 1;
    let ranBy = '';
    let tags = [];
    if(multipleOwners){
        // There are more than one "owners" of the bot
        for(let o of client.owners){
            let usr = client.users.get(o);
            let tag = usr ? usr.tag : 'Unkown user';
            tags.push(`${tag} (${o})`);
        }
        ranBy = tags.join(', ');
    } else {
        let ownerId = client.owners[0];
        let tag = client.users.get(ownerId).tag;
        ranBy = `${tag} (${ownerId})`;
    }
    const embed = new RichEmbed()
        .setTitle(`About ${client.user.tag}`)
        .setDescription('Hello, I am BaltiBot number the second. I do stuff sometimes but like idk.')
        .addField('Instance owned by', ranBy)
        .addField('NodeJS', process.version)
        .addField('Memory Usage:', `${Math.floor(process.memoryUsage().rss / 1000000)}MB\n`)
        .addBlankField()
        .addField('Report a bug/Request a feature', 'https://gitlab.com/MikeModder/BaltiBot/issues')
        .addField('Server', 'https://toram.party');
    msg.channel.send(embed);
};

exports.help = { name: 'about', usage: '{prefix}about', desc: 'Get some information about the bot', aliases: [], USER_PERMS: [], BOT_PERMS: [], public: true, premium: true };