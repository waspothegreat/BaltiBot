/* eslint no-negated-condition: 0 */
const { RichEmbed } = require('discord.js');
const { eachOf } = require('async');

exports.run = async (client, msg) => {
    const cmd = msg.args[0];
    const embed = new RichEmbed();
    if(!cmd){
        // Send a command list
        embed.setTitle('Command list');
        eachOf(client.categories, (cat, k, cb1) => {
            const commandsInCat = client.commands.filter(c => c.cat === cat);
            let cmdArr = [];
            eachOf(commandsInCat, (c, k, cb2) => {
                cmdArr.push(c[1].help.name);
                cb2();
            }, (err) => {
                if(err) return msg.replyErr(msg.channel, err);
                embed.addField(`Commands in category ${cat}:`, `\`${cmdArr.join(', ')}\``);
                cb1();
            });
        }, (err) => {
            if(err) return msg.replyErr(msg.channel, err);
            msg.channel.send(embed);
        });
    } else {
        // Get help for a specific command
        if(!client.commands.has(cmd) && !client.aliases.has(cmd)) return msg.replyErr(msg.channel, `\`${cmd}\` is not a valid command!`);
        const command = client.commands.get(cmd) || client.commands.get(client.aliases.get(cmd));
        embed
            .setTitle(`Help for command ${cmd}`)
            .addField('Usage: ', command.help.usage.replace('{prefix}', process.env.DISCORD_PREFIX) || 'This command does not have a usage field.')
            .addField('Description: ', command.help.desc || 'This command does not have a description!')
            .addField('Aliases: ', `\`${command.help.aliases.join(', ') || 'None'}\``)
            .setDescription('<> denotes an optional parameter\n[]  denotes a required one');
        msg.channel.send(embed);
    }
};

exports.help = { name: 'help', usage: '{prefix}help <command>', desc: 'Get some help', aliases: ['halp'], USER_PERMS: [], BOT_PERMS: [], public: true, premium: true };