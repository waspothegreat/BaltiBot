exports.run = async (client, msg) => {
    const members = msg.mentions.members.array();
    if(!members || members.length === 0) return msg.replyErr(msg.channel, 'You must mention at least one user to kick!');
    const reason = msg.args.join(' ') || 'No reason given';
    for(var m of members){
        if(m.user.id === msg.author.id) return msg.replyErr(msg.channel, 'You can\'t kick yourself!');
        if(m.user.id === client.user.id) return msg.replyErr(msg.channel, 'You can\'t kick me! ~~i mean why would I abuse myself like that?~~');
        if(m.user.id === msg.guild.owner.id) return msg.replyErr(msg.channel, 'You can\'t kick the server owner! That\'d just be stupid...');
        try {
            m.ban(reason);
            msg.replyOk(msg.channel, `***${m.user.tag}*** was kicked successfully!`);
        } catch (e) {
            msg.replyErr(msg.channel, `There was an error kicked ***${m.user.tag}***:\n${e}`);
        }
    }
};

exports.help = { name: 'kick', usage: '{prefix}kick [@User1] <@User2.. ect>', desc: 'Kick some stupid people', aliases: [], USER_PERMS: ['KICK_MEMBERS'], BOT_PERMS: ['KICK_MEMBERS'], public: true, premium: false };