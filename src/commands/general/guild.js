const { RichEmbed } = require('discord.js');

exports.run = async (client, msg) => {
    const embed = new RichEmbed();
    const roles = msg.guild.roles
        .filter(r => r.id !== msg.guild.id)
        .sort((a, b) => b.position - a.position)
        .map(r => r.toString())
        .join(', ') || 'None';
    const channels = msg.guild.channels
        .sort((a, b) => a.position - b.position)
        .map(c => c.toString())
        .join(', ') || 'None';
    embed
        .setTitle(`About ${msg.guild.name}`)
        .setThumbnail(msg.guild.iconURL)
        .addField('Guild ID', msg.guild.id)
        .addField('Guild owner', `${msg.guild.owner.user.tag} (ID: ${msg.guild.ownerID})`)
        .addField('Member count', msg.guild.memberCount)
        .addField(`Roles (Count: ${msg.guild.roles.size})`, roles)
        .addField(`Channels (Count: ${msg.guild.channels.size})`, channels);
    msg.channel.send(embed);
};

exports.help = { name: 'guild', usage: '{prefix}guild', desc: 'Get information about the current guild', aliases: [], USER_PERMS: [], BOT_PERMS: [], public: true };