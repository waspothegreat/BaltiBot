const { RichEmbed } = require('discord.js');

exports.run = async (client, msg) => {
    const id = msg.args[0];
    const channel = msg.mentions.channels.first() || msg.channel;
    channel.fetchMessage(id)
        .then(m => {
            const embed = new RichEmbed();
            embed
                .setAuthor(m.author.tag)
                .setDescription(m.content)
                .setTimestamp(m.editedAt ? m.editedAt : m.createdAt)
                .setColor(m.member.displayHexColor)
                .setFooter(`Message in ${channel.name} | Sent`, m.author.avatarURL);
            msg.channel.send(embed);
        })
        .catch(e => {
            msg.replyErr(msg.channel, `There was an error getting that message. Maybe it was deleted or you used an incorrect ID?\n${e}`);
        });
};

exports.help = { name: 'quote', usage: '{prefix}quote <id> [#channel]', desc: 'Quote a message.', aliases: [], USER_PERMS: [], BOT_PERMS: [], public: false, premium: false };