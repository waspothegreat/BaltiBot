const Jimp = require('jimp');
const { Attachment } = require('discord.js');

exports.run = async (client, msg) => {
    const start = Date.now();
    const attachment = msg.attachments.first();
    const passedUrl = msg.args[0];
    const fileRegex = new RegExp('.{1,}\\.(png|jpg|jpeg|bmp)');
    let validAttach, validUrl;
    if(!attachment && !passedUrl) return msg.replyErr(msg.channel, 'You need to attach a file or provide a URL to one!');
    if(passedUrl) validUrl = fileRegex.test(passedUrl);
    if(attachment) validAttach = fileRegex.test(attachment.proxyURL || '');
    if(!validAttach && !validUrl) return msg.replyErr(msg.channel, 'You must provide a image in one of the following formats:\n`PNG, JPG, BMP`');
    if(validAttach && validUrl) msg.replyWarn(msg.channel, 'You have provided both an attachment and a file URL. The passed URL has higher priority and will be used.');
    const url = passedUrl || attachment.url;
    Jimp.read(url)
        .then(image => {
            image.pixelate(5);
            image.getBuffer(Jimp.MIME_PNG, (err, buf) => {
                if(err) return msg.replyErr(msg.channel, `There was an error processing your image!\n${err}`);
                const doneAttachment = new Attachment(buf, 'pixelate.png');
                msg.channel.send(`Time taken: \`${Date.now() - start}ms\``, doneAttachment)
                    .catch(e => msg.replyErr(msg.channel, `There was an error uploading your image! It might have gotten too big!\n${e}`));
            });
        })
        .catch(e => msg.replyErr(msg.channel, `There was an error loading your image! Make sure you gave a working link! (Time taken: \`${Date.now() - start}ms\`)\n${e}`));

};

exports.help = { name: 'pixelate', usage: '{prefix}pixelate <url>', desc: 'Pixelate an image.', aliases: [], USER_PERMS: [], BOT_PERMS: [], public: true, premium: false };