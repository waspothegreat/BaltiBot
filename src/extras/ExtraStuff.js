/**
 * Updates the client's current game
 * @param {ClientQueryOptions} client The client to update
 * @returns {null} null
 */
let updateGame = (client) => {
    const locations = [
        'Sofya City',
        'Land Under Development',
        'Underground Ruins',
        'Rakau Plains',
        'Reug Salt Plains: Wanderers Camp',
        'Isthmus Of Kaus',
        'Ruined Temple Town',
        'Ruined Temple: Forbidden hall',
        'Underground Channel',
        'Rugio Ruins'
    ];
    const choices = [
        `in ${client.guilds.size} guilds!`,
        `with ${client.channels.size} channels!`,
        `with ${client.emojis.size} emojis!`,
        `Toram Online with ${client.users.size} peeps.`,
        `with ${client.users.size} users.`,
        `in ${locations[Math.floor(Math.random() * locations.length)]}`
    ];
    const newGame = choices[Math.floor(Math.random() * choices.length)];
    client.user.setActivity(`${newGame} | https://toram.party`);

};

/**
 * @param {Client} client Client to use
 * @param {String} guildId ID of guild to search
 * @param {String} search Search query
 * @returns {GuildMember[]} Array of matching guild members
 */
let findUser = (client, guildId, search) => {
    const s = search.toLowerCase();
    return client.guilds.get(guildId).members.filter(m => m.user.tag.toLowerCase().includes(s) ||
    m.displayName.toLowerCase().includes(s) ||
    m.id === s);
};

module.exports = { updateGame, findUser };
