require('dotenv').config();

const Discord = require('discord.js');
const Enmap = require('enmap');
const { readdir } = require('fs');

const client = new Discord.Client();
client.commands = new Enmap();
client.aliases = new Enmap();
//client.debug = process.env.DEBUG === 'true';

// Load the commands
const categories = process.env.DISCORD_CATEGORIES.split(',');
client.categories = categories;
for(var i = 0; i < categories.length; i++){
    const currCat = categories[i];
    readdir(`./src/commands/${categories[i]}/`, (err, files) => {
        if(err) throw err;
        if(!files) throw new Error(`No commands to load in category ${currCat}`);
        console.log(`[LOADING] Loading ${files.length} commands in category ${currCat}...`);
        files.forEach(f => {
            const cmd = require(`./src/commands/${currCat}/${f}`);
            cmd.cat = currCat;
            cmd.help.aliases.forEach(a => {
                client.aliases.set(a, cmd.help.name);
            });
            client.commands.set(cmd.help.name, cmd);
        });
    });
}

// Other configuration stuffs
client.owners = process.env.DISCORD_OWNERS.split(',');

client.on('ready', () => {
    console.log(`[INFO] Bot logged in as ${client.user.tag}`);
    client.extras = require('./src/extras/ExtraStuff.js');
    client.extras.updateGame(client);
    setInterval(() => {
        client.extras.updateGame(client);
    }, 90000);
});

client.on('message', async msg => {
    if(msg.author.bot || !msg.guild) return; // Ignore bots and anything that isn't a guild

    msg.replyErr = (c, errText, title) => {
        const embed = new Discord.RichEmbed()
            .setTitle(title || `❌ Error!`)
            .setDescription(errText)
            .setColor('RED');
        c.send(embed);
    };
    msg.replyWarn = (c, warnText, title) => {
        const embed = new Discord.RichEmbed()
            .setTitle(title || `⚠️ Warning!`)
            .setDescription(warnText)
            .setColor('ORANGE');
        c.send(embed);
    };
    msg.replyOk = (c, okText, title) => {
        const embed = new Discord.RichEmbed()
            .setTitle(title || `✅ Success!`)
            .setDescription(okText)
            .setColor('GREEN');
        c.send(embed);
    };

    if(msg.content.indexOf(process.env.DISCORD_PREFIX) !== 0) return;
    msg.args = msg.content.slice(process.env.DISCORD_PREFIX.length).trim().split(/ +/g);
    const command = msg.args.shift().toLowerCase();

    if(!client.commands.has(command) && !client.aliases.has(command)) return; // We don't have that command

    const cmd = client.commands.get(command) || client.commands.get(client.aliases.get(command));
    if(!cmd) return;

    // Check if the command isn't public
    if(!cmd.help.public && !client.owners.includes(msg.author.id)) return msg.replyErr(msg.channel, 'This is a owner-only command!');

    // Check user and bot permissions
    const missingUsrPerms = cmd.help.USER_PERMS.filter(p => !msg.channel.permissionsFor(msg.author).has(p));
    if(missingUsrPerms.length > 0) return msg.replyErr(msg.channel, `You are missing the following permissions needed to run this command:\n \`${missingUsrPerms.join(', ')}\``);

    const missingBotPerms = cmd.help.BOT_PERMS.filter(p => !msg.channel.permissionsFor(msg.guild.me).has(p));
    if(missingBotPerms.length > 0) return msg.replyErr(msg.channel, `The bot is missing the following permissions needed for this command:\n \`${missingBotPerms.join(', ')}\``);

    cmd.run(client, msg);

});

client.login(process.env.DISCORD_TOKEN);
